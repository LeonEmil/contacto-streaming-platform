const chatButton = document.getElementById("chat-button")
const chat = document.getElementById("chat")
const videoGrid = document.getElementById("video-grid")
const chatInput = document.getElementById("chat_message")
let menuIsHidden = getComputedStyle(chat).right !== "0px" ? true : false

const socket = io('/')
const myPeer = new Peer(undefined, {
    path: '/peerjs',
    host: '/',
    port: '443'
})
let myVideoStream;
const myVideo = document.createElement('video')
myVideo.muted = true;
const peers = {}
navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
}).then(stream => {
    myVideoStream = stream;
    addVideoStream(myVideo, stream)
    myPeer.on('call', call => {
        call.answer(stream)
        const video = document.createElement('video')
        call.on('stream', userVideoStream => {
            addVideoStream(video, userVideoStream)
        })
    })

    socket.on('user-connected', userId => {
        connectToNewUser(userId, stream)
    })
    // input value
    let text = $("input");
    // when press enter send message
    $('html').keydown(function (e) {
        if (e.which == 13 && text.val().length !== 0) {
            socket.emit('message', text.val());
            text.val('')
        }
    });
    socket.on("createMessage", message => {
        $("#chat-messages").append(`<li class="message"><b>Visitante</b><br/>${message}</li>`);
        scrollToBottom()
    })
})

socket.on('user-disconnected', userId => {
    if (peers[userId]) peers[userId].close()
})

myPeer.on('open', id => {
    socket.emit('join-room', ROOM_ID, id)
})

function connectToNewUser(userId, stream) {
    const call = myPeer.call(userId, stream)
    const video = document.createElement('video')
    video.className = "video"
    call.on('stream', userVideoStream => {
        addVideoStream(video, userVideoStream)
    })
    call.on('close', () => {
        video.remove()
    })

    peers[userId] = call
}

function addVideoStream(video, stream) {
    video.srcObject = stream
    video.addEventListener('loadedmetadata', () => {
        video.play()
    })
    videoGrid.append(video)
}



const scrollToBottom = () => {
    var d = $('.main__chat_window');
    d.scrollTop(d.prop("scrollHeight"));
}


const muteUnmute = () => {
    const enabled = myVideoStream.getAudioTracks()[0].enabled;
    if (enabled) {
        myVideoStream.getAudioTracks()[0].enabled = false;
        setUnmuteButton();
    } else {
        setMuteButton();
        myVideoStream.getAudioTracks()[0].enabled = true;
    }
}

const playStop = () => {
    console.log('object')
    let enabled = myVideoStream.getVideoTracks()[0].enabled;
    if (enabled) {
        myVideoStream.getVideoTracks()[0].enabled = false;
        setPlayVideo()
    } else {
        setStopVideo()
        myVideoStream.getVideoTracks()[0].enabled = true;
    }
}

const setMuteButton = () => {
    const html = `
    <i class="fas fa-microphone"></i>
    <span>Detener audio</span>
  `
    document.querySelector('.controls__mute-button').innerHTML = html;
}

const setUnmuteButton = () => {
    const html = `
    <i class="unmute fas fa-microphone-slash"></i>
    <span>Activar audio</span>
  `
    document.querySelector('.controls__mute-button').innerHTML = html;
}

const setStopVideo = () => {
    const html = `
    <i class="fas fa-video"></i>
    <span>Detener video</span>
  `
    document.querySelector('.controls__video-button').innerHTML = html;
}

const setPlayVideo = () => {
    const html = `
  <i class="stop fas fa-video-slash"></i>
    <span>Activar video</span>
  `
    document.querySelector('.controls__video-button').innerHTML = html;
}

// Resposive layout

let chatFocus = false

if (menuIsHidden) videoGrid.style = `width: ${window.innerWidth}px`
else videoGrid.style = `width: ${window.innerWidth - parseInt(getComputedStyle(chat).width.slice(0, -2))}px`

const toggleMenu = (closePosition) => {
    if (menuIsHidden) {
        chat.style = "right: 0"
        menuIsHidden = false
        videoGrid.style = `width: ${window.innerWidth + parseInt(getComputedStyle(chat).right.slice(0, -2))}px`
    }
    else {
        chat.style = `right: ${closePosition}`
        menuIsHidden = true
        videoGrid.style = `width: ${window.innerWidth}px`
    }
    document.body.style = `height: ${window.screen.availHeight}px`
    console.log(`height: ${window.screen.availHeight}px`)
}

const resizeLayout = (closePosition) => {
    if(chatFocus === false){
        chat.style = `right: ${closePosition}`
        menuIsHidden = closePosition === "0vw" ? false : true
        if (menuIsHidden) videoGrid.style = `width: ${window.innerWidth}px`
        else videoGrid.style = `width: ${window.innerWidth - parseInt(getComputedStyle(chat).width.slice(0, -2))}px`
        document.body.style = `height: ${window.screen.availHeight}px`
    }
}

chatInput.addEventListener("focus", () => {
    chatFocus = true
    console.log(chatFocus)
})

chatInput.addEventListener("focusout", () => {
    chatFocus = false
    console.log(chatFocus)
})

chatButton.addEventListener("click", () => {
    if (innerWidth < 600) {
        toggleMenu('-60vw')
    }
    if (innerWidth >= 600 && innerWidth < 960) {
        toggleMenu('-45vw')
    }
    if (innerWidth >= 960 && innerWidth < 1280) {
        toggleMenu('-25vw')
    }
    if (innerWidth > 1280) {
        toggleMenu('-30vw')
    }
})

window.addEventListener("resize", () => {
    if (innerWidth < 600) {
        resizeLayout("-60vw")
    }
    if (innerWidth > 600 && innerWidth < 960) {
        resizeLayout("-45vw")
    }
    if (innerWidth > 960) {
        resizeLayout("0vw")
    }
})